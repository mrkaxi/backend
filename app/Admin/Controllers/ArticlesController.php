<?php

namespace App\Admin\Controllers;

use App\Models\Articles;
use App\Models\ArticleCategory;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ArticlesController extends Controller
{
    use HasResourceActions;
    
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Articles);
        $grid->id('ID');
        $grid->title('标题');
        $grid->cover('封面')->display(function($con){
            if($con){
                return $con;
            }else{
                preg_match('/<img[^>]+src=[\"\']([^\"^\']+)[\"\'][^>]+>/i', $this->content,$match);
                return $match[1];
            }            
        })->image();
        $grid->content('内容')->display(function($con){
            return str_limit(strip_tags($con),400,'...');
        });
        
        $states = [
            'on' => ['text' => 'YES'],
            'off' => ['text' => 'NO'],
        ];
//        $grid->column('推荐')->switchGroup([ 'hot' => '热门文章'], $states);
        $grid->author('作者');
        $grid->catogery_id('分类')->display(function($cid){
            return ArticleCategory::find($cid)->name;
        });
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Articles::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Articles);

//        $form->display('ID');
//        $form->display('Created at');
//        $form->display('Updated at');
        $form->text('title','标题')->rules("required");
        $form->text('author','作者')->rules("required|Integer");
        $form->select('catogery_id', '分类')->options(ArticleCategory::pluck('name', 'id'));
        $form->textarea('desc','简介');
        $form->image('cover', '封面')->rules('mimes:gif,jpg,png,jpeg');
        $form->editor('content','内容')->rules("required");
        return $form;
    }
}
