<?php

namespace App\Admin\Controllers;

use App\Models\Advert;
use App\Models\AdvertType;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AdvertController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Advert);

        $grid->id('ID');
        $grid->advert_type('Advert Type')->advert_type_name();
        $grid->advert_desc('Advert Desc');
        $grid->image('Image')->image();
        $grid->url('Url');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Advert::findOrFail($id));

        $show->id('ID');
        $show->advert_desc('Advert Desc');
        $show->advert_type_id('Advert Type');
        $show->image('Image')->image();
        $show->url('Url');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Advert);

//        $form->display('ID');
        $form->text('advert_desc', 'Name')->rules("required");
        $form->select('advert_type_id', 'Advert Type')->options(AdvertType::pluck('advert_type_name', 'id'));
        $form->image('image', 'Image')->rules('mimes:gif,jpg,png,jpeg');
        $form->url('url', 'Url')->rules("required");
//        $form->display('Created at');
//        $form->display('Updated at');

        return $form;
    }
}
