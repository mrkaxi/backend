<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('users', UserController::class);
    $router->resource('advert_type', AdvertTypeController::class);
    $router->resource('advert', AdvertController::class);
    $router->resource('article_category', ArticleCategoryController::class);
    $router->resource('article', ArticlesController::class);
    $router->resource('article_posts', ArticlePostsController::class);
    $router->resource('article_hot', ArticleHotController::class);
});
