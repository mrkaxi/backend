<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articles;
use App\Models\ArticleCategory;
use App\Models\Advert;
use App\Models\ArticlePosts;
use App\Models\ArticleHot;
use App\Models\Reply;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cid=1)
    {
        $articles = Articles::where('catogery_id',$cid)->orderBy('id','desc')->paginate();
        foreach ($articles as &$a){
            if(!$a->cover){
                $a->cover = $this->getFirstImg($a->content);
            }else{
                $a->cover = Storage::disk(config('admin.upload.disk'))->url($a->cover);
            }
            $a->content   = str_limit(strip_tags($a->content),200,'...');
            $a->view_num  = $a->view_num>999?$a->view_num.'+':$a->view_num;
            $a->posts_num = $a->posts_num>999?$a->posts_num.'+':$a->posts_num;
        }
//        var_dump($articles->nextPageUrl());exit;
        $articleCategory = ArticleCategory::all();
        $rightAdverts  = Advert::where('advert_type_id',4)->get();
        $slides  = Advert::where('advert_type_id',1)->get();
        $ad1  = Advert::where('advert_type_id',2)->first();
        $ad2  = Advert::where('advert_type_id',3)->first();
        return view('articleslayout',[
            'articles'=>$articles,
            'rightAdverts'=>$rightAdverts,
            'slides'=>$slides,
            'ad1'=>$ad1,
            'ad2'=>$ad2,
            'articleCategory'=>$articleCategory
        ]);
    }
    public function getFirstImg($content){
        preg_match('/<img[^>]+src=[\"\']([^\"^\']+)[\"\'][^>]+>/i', $content,$match);
        return isset($match[1])?$match[1]:'';
    }
    public function detail($id)
    {
        $posts = ArticlePosts::where('article_id',$id)->get();
        
        $article = Articles::find($id);
        Articles::find($id)->increment('view_num');
        $replies = [];
        foreach ($posts as &$p){
            if($p->reply_num){
                $replies[$p->id] = Reply::where('post_id',$p->id)->get();//limit(5)
            }
        }
        $hots = ArticleHot::all();
        foreach ($hots as &$h){
            if(!$h->cover){
                $h->cover = $this->getFirstImg($h->ArticleInfo->content);
            }else{
                $h->cover = Storage::disk(config('admin.upload.disk'))->url($h->cover);
            }
            $h->ArticleInfo->content = str_limit(strip_tags($h->ArticleInfo->content),400,'...');
        }
        return view('articledetaillayout',[
            'article'=>$article,
            'posts'=>$posts,
            'hots'=>$hots,
            'replies'=>$replies
        ]);
    }
}
