<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleHot extends Model
{
    protected $table = 'articles_hot';
    
    public function ArticleInfo()
    {
        return $this->hasOne(Articles::class, 'id', 'article_id');
    }
}
