<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Articles;
use App\Models\Reply;
use Carbon\Carbon;

class ArticlePosts extends Model
{
    protected $table = 'article_posts';
    
    public static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            Articles::find($model->article_id)->increment('posts_num');
        });
    }
    public function article()
    {
        return $this->belongsTo(Articles::class, 'article_id', 'id');
    }
    public function prePost()
    {
        return $this->hasOne(self::class,'id','parent_id');
    }
    public function replies()
    {
        return $this->hasManay(Reply::class,'post_id');
    }
    public function getCreatedAtAttribute($date) {
        if (Carbon::now() > Carbon::parse($date)->addDays(1)) {
            return Carbon::parse($date);
        }

        return Carbon::parse($date)->diffForHumans();
    }
}
