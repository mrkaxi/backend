<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Articles extends Model
{
    protected $table = 'articles';
    
    public function articleCatogery()
    {
        return $this->belongsTo(ArticleCategory::class,'catogery_id','id');
    }
    public function posts()
    {
        return $this->hasManay(ArticlePosts::class,'article_id','id');
    }
    public function getCreatedAtAttribute($date) {
        if (Carbon::now() > Carbon::parse($date)->addDays(1)) {
            return Carbon::parse($date);
        }

        return Carbon::parse($date)->diffForHumans();
    }
}
