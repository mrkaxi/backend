<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ArticlePosts;

class Reply extends Model
{
    protected $table = 'reply';
    public static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            ArticlePosts::find($model->post_id)->increment('reply_num');
        });
    }
    public function post()
    {
        return $this->belongsTo(ArticlePosts::class, 'post_id');
    }
}
