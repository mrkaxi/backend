@extends('layouts.common')
@section('content')
    <div class="mob-container">
        <div class="mob-row">
            <div class="mob-col8">
                <div class="details-main">
                    <div class="title">{{$article->title}}</div>
                    <div class="small">
                        <div class="s-left">
                            <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                            Harinder Bharwal · 互联网 · {{$article->created_at}}
                        </div>
                        <div class="record">
                            和朋友分享：<a href="" class="g-icon microblog"></a><a href="" class="g-icon wx"></a><a href=""
                                class="g-icon qq"></a>
                        </div>
                    </div>
                    <div class="details-bd">
                        <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                        {!!$article->content!!}
                    </div>
                    <div class="share"></div>
                </div>
                <div class="share-friend">
                    <div class="btns-wrap"><button>分享给朋友<i class="g-icon"></i></button></div>
                    <div class="statement"><a href="#">举报文章</a>免责声明：本文来自网站用户贡献，不代表本网站的观点和立场。</div>
                </div>
                <div class="details-main-ad"><a href=""><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></a></div>
                <div class="recommend">
                    <div class="details-main-b-title">推荐文章</div>
                    <ul class="mob-row">
                        <li class="mob-col4">
                            <a href="">
                                <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                <div class="title">明天，Switch 版的《星露谷物语》将迎来「多人模式」</div>
                                <div class="record">
                                    <div class="tiem">3小时</div>
                                    <div class="right">
                                        <span class="browse"><i class="g-icon"></i>999+</span>
                                        <span class="comment"><i class="g-icon"></i>999+</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="mob-col4">
                            <a href="">
                                <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                <div class="title">明天，Switch 版的《星露谷物语》将迎来「多人模式」</div>
                                <div class="record">
                                    <div class="tiem">3小时</div>
                                    <div class="right">
                                        <span class="browse"><i class="g-icon"></i>999+</span>
                                        <span class="comment"><i class="g-icon"></i>999+</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="mob-col4">
                            <a href="">
                                <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                <div class="title">明天，Switch 版的《星露谷物语》将迎来「多人模式」</div>
                                <div class="record">
                                    <div class="tiem">3小时</div>
                                    <div class="right">
                                        <span class="browse"><i class="g-icon"></i>999+</span>
                                        <span class="comment"><i class="g-icon"></i>999+</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="details-comment">
                    <div class="loging"><a href=""><i class="g-icon"></i>登录后评论</a></div>
                    <div class="comment-box">
                        <form method="POST" action="{{ route('new') }}">
                        {{ csrf_field() }}
                        <textarea rows="10"></textarea>
                        <input type="hidden" class="form-control" name="id" value="{{ $article->id }}">
                        <button type="submit" >发布</button>
                        </form>
                        @guest
                        <button onclick="window.location.href='{{ route('login') }}'">登录</button>
                        @endguest
                    </div>
                    <div class="have-comment">
                        <div class="title">
                            <h4>全部评论（{{$article->posts_num}}）</h4>
                            <div class="tabs">
                                <a href="" class="on">最新</a>
                                <a href="">最热</a>
                            </div>
                        </div>
                        <div class="comment-list">
                            <ul>
                                @foreach ($posts as $post)
                                <li>
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="right">
                                        <div class="user">
                                            <span class="name">Kwak Seong-Min</span>
                                            <span class="tiem">{{$post->created_at}}</span>
                                        </div>
                                        <div class="comment-text">{{$post->content}}</div>
                                        <div class="info">
                                            <div class="zan"><i class="g-icon"></i>{{$post->like_num}}赞</div>
                                            <div class="reply"><i class="g-icon"></i>{{$post->reply_num}}赞</div>
                                        </div>
                                        @if ($post->reply_num)
                                        <ul class="reply-wrap">
                                            @foreach ($replies[$post->id] as $reply)
                                            <li>
                                                <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                <div class="right">
                                                    <div class="user">
                                                        <span class="name">Kwak Seong-Min</span>
                                                        <span class="tiem">{{$reply->created_at}}</span>
                                                    </div>
                                                    <div class="comment-text">{{$reply->content}}</div>
                                                    <div class="info">
                                                        <div class="zan"><i class="g-icon"></i>{{$reply->like_num}}赞</div>
                                                        <div class="reply"><i class="g-icon"></i>{{$reply->reply_num}}赞</div>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </div>
                                </li>
                                @endforeach
<!--                                <li>
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="right">
                                        <div class="user">
                                            <span class="name">Kwak Seong-Min</span>
                                            <span class="tiem">16分钟前</span>
                                        </div>
                                        <div class="comment-text">二是造新门槛。众所周知，作为飞机的心脏，动力引擎直接影响着飞机的性能、可靠性及经济性，技术门槛很高，因此也长期处于技术垄断状态。</div>
                                        <div class="info">
                                            <div class="zan"><i class="g-icon"></i>3赞</div>
                                            <div class="reply"><i class="g-icon"></i>3赞</div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="right">
                                        <div class="user">
                                            <span class="name">Kwak Seong-Min</span>
                                            <span class="tiem">16分钟前</span>
                                        </div>
                                        <div class="comment-text">二是造新门槛。众所周知，作为飞机的心脏，动力引擎直接影响着飞机的性能、可靠性及经济性，技术门槛很高，因此也长期处于技术垄断状态。</div>
                                        <div class="info">
                                            <div class="zan"><i class="g-icon"></i>3赞</div>
                                            <div class="reply"><i class="g-icon"></i>3赞</div>
                                        </div>
                                        <ul class="reply-wrap">
                                            <li>
                                                <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                <div class="right">
                                                    <div class="user">
                                                        <span class="name">Kwak Seong-Min</span>
                                                        <span class="tiem">16分钟前</span>
                                                    </div>
                                                    <div class="comment-text">二是造新门槛。众所周知，作为飞机的心脏，动力引擎直接影响着飞机的性能、可靠性及经济性，技术门槛很高，因此也长期处于技术垄断状态。</div>
                                                    <div class="info">
                                                        <div class="zan"><i class="g-icon"></i>3赞</div>
                                                        <div class="reply"><i class="g-icon"></i>3赞</div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mob-col4">
                <div class="right-ad">
                    <ul class="ad-list">
                        <li>
                            <a href="" class="picture">
                                <img src="{{ asset('statics/images/mob4.jpg') }}" alt="">
                            </a>
                        </li>
                    </ul>
                    <div class="hot-article">
                        <div class="title">热门文章</div>
                        <ul class="">
                            @foreach ($hots as $hot)
                            <li>
                                <a href="{{ route('new',['id'=>$hot->article_id]) }}">
                                    <div class="picture"><img src="{{$hot->cover}}" alt=""></div>
                                    <div class="text">{{$hot->ArticleInfo->content}}</div>
                                </a>
                            </li>
                            @endforeach
<!--                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>-->
                        </ul>
                        <div class="more"><button>查看更多</button></div>
                    </div>
                    <div class="contact-us">
                        <p class="tc">
                            广告投放 | 站点统计<br>安卓巴士 ( 粤ICP备15117877号 )
                        </p>
                        <div class="qrcode tc">
                            <div><img src="/src/assets/images/qrcode.png" alt=""></div>
                            <p>安卓巴士公众号</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="left-suspend">
        <div class="share">
            <div class="label">分享</div>
            <ul>
                <li class="microblog">
                    <i class="g-icon"></i>
                    微博
                </li>
                <li class="wx">
                    <i class="g-icon"></i>
                    微信
                </li>
                <li class="qq">
                    <i class="g-icon"></i>
                    QQ
                </li>
            </ul>
        </div>
        <div class="comment">
            <i class="g-icon"></i>
            评论
        </div>
    </div>
@endsection