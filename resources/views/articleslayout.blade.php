@extends('layouts.common')
@section('content')
    <div class="banner-wrap">
        <div class="mob-container">
            <div class="mob-row">
                <div class="mob-col8">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach ($slides as $slide)
                                <div class="swiper-slide">
                                    <div class="picture"><img src="{{ Storage::disk(config('admin.upload.disk'))->url($slide->image) }}" alt=""></div>
                                    <a class="text">{{ $slide->title }}</a>
                                </div>
                            @endforeach
<!--                            <div class="swiper-slide">
                                <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                <a class="text">苹果选出了 5 个最好的国产 App，我们和获奖的开发者聊了聊</a>
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('statics/images/mob4.jpg') }}" alt="">
                                <a class="text">苹果选出了 5 个最好的国产 App，我们和获奖的开发者聊了聊</a>
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('statics/images/mob4.jpg') }}" alt="">
                                <a class="text">苹果选出了 5 个最好的国产 App，我们和获奖的开发者聊了聊</a>
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('statics/images/mob4.jpg') }}" alt="">
                                <a class="text">苹果选出了 5 个最好的国产 App，我们和获奖的开发者聊了聊</a>
                            </div>-->
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="mob-col4">
                    <div class="banner-right">
                        <div class="hot-picture">
                            <img src="{{ Storage::disk(config('admin.upload.disk'))->url($ad1->image) }}" alt="">
                            <p class="info"><a href="{{ $ad1->url }}">{{ $ad1->advert_desc }}</a></p>
                        </div>
                    </div>
                    <div class="banner-right">
                        <div class="hot-picture">
                            <img src="{{ Storage::disk(config('admin.upload.disk'))->url($ad2->image) }}" alt="">
                            <p class="info"><a href="{{ $ad2->url }}">{{ $ad2->advert_desc }}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mob-container">
        <div class="main-nav">
            <ul>
                @foreach ($articleCategory as $acs)
                    <li class="{{Route::input('cid')==$acs->id?'on':''}}"><a href="{{ route('news',['cid'=>$acs->id]) }}">{{$acs->name}}</a></li>
                @endforeach
                
<!--                <li class="on"><a href="">推荐文章</a></li>
                <li><a href="">热点</a></li>
                <li><a href="">科技</a></li>
                <li><a href="">互联网</a></li>
                <li><a href="">数码</a></li>
                <li><a href="">游戏</a></li>
                <li><a href="">历史</a></li>
                <li><a href="">娱乐</a></li>
                <li><a href="">其他</a></li>-->
            </ul>
        </div>
        <div class="mob-row">
            <div class="mob-col8 ">
                <div class="left-lists">
                    <div class="list-top">
                        <ul>
                            <li class="on"><a href="">最新</a></li>
                            <li><a href="">最热</a></li>
                        </ul>
                    </div>
                    <div class="list-main">
                        <ul>
                            @foreach ($articles as $article)
                                    <li>
                                        <a href="{{ route('new',['id'=>$article->id]) }}">
                                            <div class="picture"><img src="{{ $article->cover }}" alt=""></div>
                                            <div class="list-info">
                                                <div class="title">{{ $article->title }}</div>
                                                <div class="text">{{ $article->content }}</div>
                                                <div class="small">
                                                    <div class="s-left">
                                                        <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                        Harinder Bharwal · {{ $article->articleCatogery->name }} · {{$article->created_at}}
                                                    </div>
                                                    <div class="record">
                                                        <span class="browse"><i class="g-icon"></i>{{ $article->view_num }}</span>
                                                        <span class="comment"><i class="g-icon"></i>{{ $article->posts_num }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                            @endforeach
<!--                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="list-info">
                                        <div class="title">Mac 用户等足五年，幸亏这个办公神器没有让人失望，五年过去了，第一个 Mac 上的 WPS 才姗姗来迟。</div>
                                        <div class="text">5月7日至8日，习近平同金正恩在大连举行会晤。这是今年3月底以来中朝两国领导人第二次会晤。40多天…</div>
                                        <div class="small">
                                            <div class="s-left">
                                                <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                Harinder Bharwal · 互联网 · 3小时前
                                            </div>
                                            <div class="record">
                                                <span class="browse"><i class="g-icon"></i>999+</span>
                                                <span class="comment"><i class="g-icon"></i>999+</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="list-info">
                                        <div class="title">星际迷航的序曲：「离子风」无人机究竟带来了什么？</div>
                                        <div class="text">5月7日至8日，习近平同金正恩在大连举行会晤。这是今年3月底以来中朝两国领导人第二次会晤。40多天…</div>
                                        <div class="small">
                                            <div class="s-left">
                                                <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                Harinder Bharwal · 互联网 · 3小时前
                                            </div>
                                            <div class="record">
                                                <span class="browse"><i class="g-icon"></i>999+</span>
                                                <span class="comment"><i class="g-icon"></i>999+</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>-->
                        </ul>
<!--                        <div class="list-ad">
                            <a href="">
                                <div class="picture">
                                    <img src="{{ asset('statics/images/mob4.jpg') }}" alt="">
                                    <div class="text">
                                        <div class="title">马自达：电气化可以，但放弃燃油机不行</div>
                                        <div class="s-left">
                                            <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                            Harinder Bharwal · 互联网 · 3小时前
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>-->
<!--                        <ul>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="list-info">
                                        <div class="title">Mac 用户等足五年，幸亏这个办公神器没有让人失望，五年过去了，第一个 Mac 上的 WPS 才姗姗来迟。</div>
                                        <div class="text">5月7日至8日，习近平同金正恩在大连举行会晤。这是今年3月底以来中朝两国领导人第二次会晤。40多天…</div>
                                        <div class="small">
                                            <div class="s-left">
                                                <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                Harinder Bharwal · 互联网 · 3小时前
                                            </div>
                                            <div class="record">
                                                <span class="browse"><i class="g-icon"></i>999+</span>
                                                <span class="comment"><i class="g-icon"></i>999+</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                    <div class="list-info">
                                        <div class="title">星际迷航的序曲：「离子风」无人机究竟带来了什么？</div>
                                        <div class="text">5月7日至8日，习近平同金正恩在大连举行会晤。这是今年3月底以来中朝两国领导人第二次会晤。40多天…</div>
                                        <div class="small">
                                            <div class="s-left">
                                                <div class="h-portrait"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                                                Harinder Bharwal · 互联网 · 3小时前
                                            </div>
                                            <div class="record">
                                                <span class="browse"><i class="g-icon"></i>999+</span>
                                                <span class="comment"><i class="g-icon"></i>999+</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>-->
                        <div class="list-last">
                            <button>加载更多</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mob-col4">
                <div class="right-ad">
                    <ul class="ad-list">
                        @foreach ($rightAdverts as $ra)
                        <li>
                            <a href="{{ $ra->url }}" class="picture">
                                <img src="{{ Storage::disk(config('admin.upload.disk'))->url($ra->image) }}" alt="">
                            </a>
                        </li>
                        @endforeach
<!--                        <li>
                            <a href="" class="picture">
                                <img src="{{ asset('statics/images/mob4.jpg') }}" alt="">
                            </a>
                        </li>-->
                    </ul>
                    <div class="contact-us">
                        <p class="tc">
                            广告投放 | 站点统计<br>安卓巴士 ( 粤ICP备15117877号 )
                        </p>
                        <div class="qrcode tc">
                            <div><img src="{{ asset('statics/images/qrcode.gif') }}" alt=""></div>
                            <p>安卓巴士公众号</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection