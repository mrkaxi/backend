<!DOCTYPE html>
<html>

<head>
    <title>安卓巴士</title>
    <meta charset="utf-8">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta content=always name=referrer>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no">
    <meta name="full-screen" content="yes">
    <meta name="x5-page-mode" content="app">
    <meta content="yes">
    <meta content="black">
    <meta content="telephone=no,email=no">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/page.css') }}" rel="stylesheet">
    <link href="{{ asset('css/swiper.min.css') }}" rel="stylesheet">

</head>
<body style="background:#fff;">
    <div class="header-wrap">
        <div class="left">
            <div class="logo">
                <span class="logo-title">巴士资讯</span>
                <span class="explain">提供最新的行业资讯</span>
            </div>
            <div class="head-nav">
                <ul>
                    <li><span class="g-icon i1"></span>资讯</li>
                    <li><span class="g-icon i2"></span>论坛</li>
                </ul>
            </div>
        </div>
        <div class="right">
            <div class="search">
                <input type="text" placeholder="请输入搜索内容">
                <span class="g-icon"></span>
            </div>
            <div class="user">
                <a href="javascript:;">登录</a>
                <a href="javascript:;">注册</a>
            </div>
        </div>
    </div>
    <div class="mob-container">
        <div class="mob-row">
            <div class="mob-col8">
                <div class="details-main">
                    <div class="title">{{$article->title}}</div>
                    <div class="small">
                        <div class="s-left">
                            <div class="h-portrait"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                            Harinder Bharwal · 互联网 · 3小时前
                        </div>
                        <div class="record">
                            和朋友分享：<a href="" class="g-icon microblog"></a><a href="" class="g-icon wx"></a><a href=""
                                class="g-icon qq"></a>
                        </div>
                    </div>
                    <div class="details-bd">
                        <div class="picture"><img src="{{ asset('statics/images/mob4.jpg') }}" alt=""></div>
                        {!!$article->content!!}
                    </div>
                    <div class="share"></div>
                </div>
                <div class="share-friend">
                    <div class="btns-wrap"><button>分享给朋友<i class="g-icon"></i></button></div>
                    <div class="statement"><a href="#">举报文章</a>免责声明：本文来自网站用户贡献，不代表本网站的观点和立场。</div>
                </div>
                <div class="details-main-ad"><a href=""><img src="/src/assets/images/mob4.jpg" alt=""></a></div>
                <div class="recommend">
                    <div class="details-main-b-title">推荐文章</div>
                    <ul class="mob-row">
                        <li class="mob-col4">
                            <a href="">
                                <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                <div class="title">明天，Switch 版的《星露谷物语》将迎来「多人模式」</div>
                                <div class="record">
                                    <div class="tiem">3小时</div>
                                    <div class="right">
                                        <span class="browse"><i class="g-icon"></i>999+</span>
                                        <span class="comment"><i class="g-icon"></i>999+</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="mob-col4">
                            <a href="">
                                <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                <div class="title">明天，Switch 版的《星露谷物语》将迎来「多人模式」</div>
                                <div class="record">
                                    <div class="tiem">3小时</div>
                                    <div class="right">
                                        <span class="browse"><i class="g-icon"></i>999+</span>
                                        <span class="comment"><i class="g-icon"></i>999+</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="mob-col4">
                            <a href="">
                                <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                <div class="title">明天，Switch 版的《星露谷物语》将迎来「多人模式」</div>
                                <div class="record">
                                    <div class="tiem">3小时</div>
                                    <div class="right">
                                        <span class="browse"><i class="g-icon"></i>999+</span>
                                        <span class="comment"><i class="g-icon"></i>999+</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="details-comment">
                    <div class="loging"><a href=""><i class="g-icon"></i>登录后评论</a></div>
                    <div class="comment-box">
                        <textarea rows="10"></textarea>
                        <button>登录</button>
                    </div>
                    <div class="have-comment">
                        <div class="title">
                            <h4>全部评论（7）</h4>
                            <div class="tabs">
                                <a href="" class="on">最新</a>
                                <a href="">最热</a>
                            </div>
                        </div>
                        <div class="comment-list">
                            <ul>
                                <li>
                                    <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                    <div class="right">
                                        <div class="user">
                                            <span class="name">Kwak Seong-Min</span>
                                            <span class="tiem">16分钟前</span>
                                        </div>
                                        <div class="comment-text">二是造新门槛。众所周知，作为飞机的心脏，动力引擎直接影响着飞机的性能、可靠性及经济性，技术门槛很高，因此也长期处于技术垄断状态。</div>
                                        <div class="info">
                                            <div class="zan"><i class="g-icon"></i>3赞</div>
                                            <div class="reply"><i class="g-icon"></i>3赞</div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                    <div class="right">
                                        <div class="user">
                                            <span class="name">Kwak Seong-Min</span>
                                            <span class="tiem">16分钟前</span>
                                        </div>
                                        <div class="comment-text">二是造新门槛。众所周知，作为飞机的心脏，动力引擎直接影响着飞机的性能、可靠性及经济性，技术门槛很高，因此也长期处于技术垄断状态。</div>
                                        <div class="info">
                                            <div class="zan"><i class="g-icon"></i>3赞</div>
                                            <div class="reply"><i class="g-icon"></i>3赞</div>
                                        </div>
                                        <ul class="reply-wrap">
                                            <li>
                                                <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                                <div class="right">
                                                    <div class="user">
                                                        <span class="name">Kwak Seong-Min</span>
                                                        <span class="tiem">16分钟前</span>
                                                    </div>
                                                    <div class="comment-text">二是造新门槛。众所周知，作为飞机的心脏，动力引擎直接影响着飞机的性能、可靠性及经济性，技术门槛很高，因此也长期处于技术垄断状态。</div>
                                                    <div class="info">
                                                        <div class="zan"><i class="g-icon"></i>3赞</div>
                                                        <div class="reply"><i class="g-icon"></i>3赞</div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mob-col4">
                <div class="right-ad">
                    <ul class="ad-list">
                        <li>
                            <a href="" class="picture">
                                <img src="/src/assets/images/mob4.jpg" alt="">
                            </a>
                        </li>
                    </ul>
                    <div class="hot-article">
                        <div class="title">热门文章</div>
                        <ul class="">
                            <li>
                                <a href="">
                                    <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="picture"><img src="/src/assets/images/mob4.jpg" alt=""></div>
                                    <div class="text">圣诞老人追踪器正式上线！美国这 60 多年的传统从何而来？</div>
                                </a>
                            </li>
                        </ul>
                        <div class="more"><button>查看更多</button></div>
                    </div>
                    <div class="contact-us">
                        <p class="tc">
                            广告投放 | 站点统计<br>
                            � 安卓巴士 ( 粤ICP备15117877号 )
                        </p>
                        <div class="qrcode tc">
                            <div><img src="/src/assets/images/qrcode.png" alt=""></div>
                            <p>安卓巴士公众号</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="left-suspend">
        <div class="share">
            <div class="label">分享</div>
            <ul>
                <li class="microblog">
                    <i class="g-icon"></i>
                    微博
                </li>
                <li class="wx">
                    <i class="g-icon"></i>
                    微信
                </li>
                <li class="qq">
                    <i class="g-icon"></i>
                    QQ
                </li>
            </ul>
        </div>
        <div class="comment">
            <i class="g-icon"></i>
            评论
        </div>
    </div>
    <div class="right-suspend">
        <div class="contact-us g-icon"></div>
        <div class="go-top"></div>
    </div>
    <script src="/src/assets/libs/jquery-2.1.0.min.js"></script>
    <script src="/src/dist/js/common.js"></script>
</body>

</html>