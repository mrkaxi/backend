<!DOCTYPE html>
<html>

<head>
    <title>安卓巴士</title>
    <meta charset="utf-8">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta content=always name=referrer>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no">
    <meta name="full-screen" content="yes">
    <meta name="x5-page-mode" content="app">
    <meta content="yes">
    <meta content="black">
    <meta content="telephone=no,email=no">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/page.css') }}" rel="stylesheet">
    <link href="{{ asset('css/swiper.min.css') }}" rel="stylesheet">

</head>

<body>
    <div class="header-wrap">
        <div class="left">
            <div class="logo">
                <span class="logo-title" onclick="window.location='/news'" style="cursor: pointer">巴士资讯</span>
                <span class="explain">提供最新的行业资讯</span>
            </div>
            <div class="head-nav">
                <ul>
                    <li><a href="/news"><span class="g-icon i1"></span>资讯</a></li>
                    <li><span class="g-icon i2"></span>论坛</li>
                </ul>
            </div>
        </div>
        <div class="right">
            <div class="search">
                <input type="text" placeholder="请输入搜索内容">
                <span class="g-icon"></span>
            </div>
            @guest
                <div class="user">
                    <a href="{{ route('login') }}">登录</a>
                    <a href="{{ route('register') }}">注册</a>
                </div>
            @else
                <div class="user">
                    <a href="#">{{ Auth::user()->name }}</a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">退出</a>
                </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endguest
            
        </div>
    </div>
    @yield('content')
    <div class="right-suspend">
        <div class="contact-us"><i class="g-icon"></i>联系我们</div>
        <div class="go-top"></div>
    </div>
    <script src="{{ asset('js/jquery-2.1.0.min.js') }}"></script>
    <script src="{{ asset('js/swiper.min.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            autoplay: 3000,
            paginationClickable: true,
            pagination: '.swiper-pagination',
        });
    </script>
</body>

</html>