
$(function () {
    $('.go-top').on('click', function () {
        $('body,html').animate({ scrollTop: 0 }, 500);
    });

    $('.left-suspend').find('.label').on('click', function () {
        var o = $(this);
        var op = o.parents('.left-suspend');
        if (op.hasClass('on')) {
            op.removeClass('on');
        } else {
            op.addClass('on');
        }
    })
})
